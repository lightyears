#
# 20,000 Light Years Into Space
# This game is licensed under GPL v2, and copyright (C) Jack Whitham 2006-07.
#

# A model for the movement of steam within the system.


from primitives import *


class Voltage_Model:
    # Don't understand steam? Confused by the thought of
    # boiling water being used as a source of energy?
    # Why not just assume that it's electricity.

    def __init__(self,node):
        # Invariant:
        self.capacitance = 1.0
        # May change:
        self.charge = 0.0
        self.voltage = 0.0
        # Changed by upgrades
        self.capacity = INITIAL_NODE_CAPACITY
        self.node = node

    NEGLIGIBLE = 0.05
    TIME_CONSTANT = 0.1

    def Source(self, current):
        dq = current * self.TIME_CONSTANT
        self.charge += dq
        self.__Bound()

    def Think(self, neighbour_list):
        self.voltage = self.charge / self.capacitance
        currents = []

        for (neighbour, resist, max_intensity) in neighbour_list:
            dir = 0
            # Potential difference:
            dv = self.voltage - neighbour.voltage
            if ( dv >= self.NEGLIGIBLE ):
                # Current flow:
                if (resist<1.0):
                  resist=1.0

                i = dv / resist
                # Charge flow:
                dq = i * self.TIME_CONSTANT

                adq=math.fabs(dq);
                if (adq>1): dq/=math.sqrt(adq)  # Limit accurracy problems
                # Apply Intensity Limit
                if (dq>max_intensity*self.TIME_CONSTANT):
                    dq=max_intensity*self.TIME_CONSTANT

                if (dq<-max_intensity*self.TIME_CONSTANT):
                    dq=-max_intensity*self.TIME_CONSTANT

                if ( self.charge-dq > self.capacity ):
                    dq=self.charge-self.capacity
                if ( neighbour.charge+dq > neighbour.capacity ):
                    dq=neighbour.capacity-neighbour.charge

                i = dq / self.TIME_CONSTANT


                self.charge -= dq
                neighbour.charge += dq
                currents.append(i)
            else:
                currents.append(0.0)
        self.__Bound()
        return currents

    def __Bound(self):
        original_charge=self.charge
        if ( self.charge < 0 ):
            self.charge = 0
        elif ( self.charge > self.capacity ):
            self.charge = self.capacity # vent

        diff_charge=original_charge-self.charge

        if len(self.node.pipes)>0:
            diff_charge/=len(self.node.pipes)

            for pipe in self.node.pipes:
                if pipe.n1==self.node: dest=pipe.n2
                if pipe.n2==self.node: dest=pipe.n1
                dest.steam.charge+=diff_charge



    def Get_Pressure(self):
        return self.charge

    def Get_Capacity(self):
        return self.capacity

    def Capacity_Upgrade(self):
        self.capacity += CAPACITY_UPGRADE

class Steam_Model(Voltage_Model):
    pass

